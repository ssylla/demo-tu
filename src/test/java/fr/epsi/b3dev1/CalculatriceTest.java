package fr.epsi.b3dev1;

import org.junit.Assert.*;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class CalculatriceTest {
	
	@Test
	public void testSomme2Pos() {
		
		//A -> Arrangement
		Calculatrice calculatrice = new Calculatrice();
		int a = 8;
		int b = 1;
		
		//A -> Acting
		int result = calculatrice.somme(a, b);
		
		//A -> Assert : s'assurer que le résultat obtenu dans result corresponde à la valeur souhaitée
		assertEquals(9, result);
	}
	
	@Test
	public void testSomme2Neg() {
		
		//A -> Arrangement
		Calculatrice calculatrice = new Calculatrice();
		int a = -8;
		int b = -12;
		
		//A -> Acting
		int result = calculatrice.somme(a, b);
		
		//A -> Assert : s'assurer que le résultat obtenu dans result corresponde à la valeur souhaitée
		assertEquals(-20, result);
	}
	
	@Test
	public void testSomme2Null() {
		
		//A -> Arrangement
		Calculatrice calculatrice = new Calculatrice();
		int a = 0;
		int b = 0;
		
		//A -> Acting
		int result = calculatrice.somme(a, b);
		
		//A -> Assert : s'assurer que le résultat obtenu dans result corresponde à la valeur souhaitée
		assertEquals(0, result);
	}
	
	@Test
	public void testSommeAPBM() {
		
		//A -> Arrangement
		Calculatrice calculatrice = new Calculatrice();
		int a = 15;
		int b = -20;
		
		//A -> Acting
		int result = calculatrice.somme(a, b);
		
		//A -> Assert : s'assurer que le résultat obtenu dans result corresponde à la valeur souhaitée
		assertEquals(-5, result);
	}
}
