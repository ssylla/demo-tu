package fr.epsi.b3dev1;

public class Calculatrice {
	public int somme( int a, int b ) {
		
		if ( b > 0 ) {
			while ( b-- != 0 ) {
				a++;
			}
		} else if ( b < 0 ) {
			while ( b++ != 0 ) {
				a--;
			}
		}
		return a;
	}
}
